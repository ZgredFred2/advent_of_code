let string_to_char_list s = List.init (String.length s) (String.get s)
let input = Advent.read_lines "input/08sample.in" |> List.map string_to_char_list
let split_by_space = Str.split (Str.regexp " ")

let input_to_array input =
  let n = input |> List.length in
  let m = List.hd input |> List.length in
  let arr = Array.make_matrix n m 0 in
  let rec populate_row i line arr =
    match line with
    | [] -> arr
    | h :: t ->
      let value = int_of_string (String.make 1 h) in
      let () = arr.(i) <- value in
      populate_row (i + 1) t arr
  in
  let rec populate_matrix i lines arr =
    match lines with
    | [] -> arr
    | h :: t ->
      let row = populate_row 0 h arr.(i) in
      let () = arr.(i) <- row in
      populate_matrix (i + 1) t arr
  in
  populate_matrix 0 input arr
;;

let arr = input_to_array input
let add_vec (x, y) (dx, dy) = x + dx, y + dy
let rev_vec (x, y) = y, x
let mul_vec m (x, y) = x * m, y * m
let starting_point axis_move p i = axis_move |> mul_vec i |> add_vec p

let int_of_bool b =
  if b then
    1
  else
    0
;;

let calculate arr =
  let n = Array.length arr in
  (* this calcuate data for `res_arr` based on `arr` *)
  let save_highest (step_delta, start_point, end_point, axis_move) arr res_arr =
    let rec aux arr res_arr i end_point highest =
      if i = end_point then
        (* end point is exclusive *)
        res_arr
      else (
        let y, x = i in
        let () = res_arr.(y).(x) <- highest in
        let highest = max highest arr.(y).(x) in
        aux arr res_arr (add_vec i step_delta) end_point highest
      )
    in
    (* List.mapi better *)
    List.init n Fun.id
    |> List.fold_left
         (fun prev curr ->
           let start_point = starting_point axis_move start_point curr in
           let end_point = starting_point axis_move end_point curr in
           aux arr prev start_point end_point (-1))
         res_arr
  in
  (* lr, rl, tb, bt *)
  (* (delta_y, delta_x), (start_offset) , (end_offset), axis_move *)
  (* point exclusive*)
  let prespectives =
    [
      (* l->r *)
      (0, 1), (0, 0), (0, n), (1, 0);
      (* l<-r *)
      (0, -1), (0, n - 1), (0, -1), (1, 0);
      (* t->b *)
      (1, 0), (0, 0), (n, 0), (0, 1);
      (* t<-b *)
      (-1, 0), (n - 1, 0), (-1, 0), (0, 1);
    ]
  in

  (* calculate for perspective list *)
  let rec aux arr exec_list =
    match exec_list with
    | [] -> []
    | h :: t ->
      let result_array = Array.make_matrix n n (-1) in
      let result_array = save_highest h arr result_array in
      result_array :: aux arr t
  in

  aux arr prespectives
;;

let res = calculate arr

let calculate2 arr =
  let n = Array.length arr in
  let m = arr.(0) |> Array.length in
  (* this calcuate data for `res_arr` based on `arr` *)
  let save_highest (step_delta, start_point, end_point, axis_move) arr res_arr =
    let rec aux arr res_arr i end_point highest streak =
      if i = end_point then
        (* end point is exclusive *)
        res_arr
      else (
        let y, x = i in
        let () = res_arr.(y).(x) <- res_arr.(y).(x) * streak in
        let current_height = arr.(y).(x) in
        if highest <= current_height then
          (* reset *)
          aux
            arr
            res_arr
            (add_vec i step_delta)
            end_point
            current_height
            1 (*count me aswell*)
        else
          (* keep going *)
          aux arr res_arr (add_vec i step_delta) end_point highest (streak + 1)
      )
    in
    (* List.mapi better *)
    List.init n Fun.id
    |> List.fold_left
         (fun prev curr ->
           let start_point = starting_point axis_move start_point curr in
           let end_point = starting_point axis_move end_point curr in
           aux arr prev start_point end_point (-1) 0)
         res_arr
  in
  (* lr, rl, tb, bt *)
  (* (delta_y, delta_x), (start_offset) , (end_offset), axis_move *)
  (* point exclusive*)
  let prespectives =
    [
      (* l->r *)
      (0, 1), (0, 0), (0, n), (1, 0);
      (* l<-r *)
      (0, -1), (0, n - 1), (0, -1), (1, 0);
      (* t->b *)
      (1, 0), (0, 0), (n, 0), (0, 1);
      (* t<-b *)
      (-1, 0), (n - 1, 0), (-1, 0), (0, 1);
    ]
  in

  (* calculate for perspective list *)
  let rec aux arr exec_list acc =
    match exec_list with
    (* | [] -> [] *)
    (* | h :: t -> *)
    (*   let new_arr = Array.make_matrix n m 1 in *)
    (*   let result_array = save_highest h arr new_arr in *)
    (*   result_array :: aux arr t result_array *)
    | [] -> acc
    | h :: t ->
      let result_array = save_highest h arr acc in
      aux arr t result_array
  in

  let new_arr = Array.make_matrix n m 1 in

  aux arr prespectives new_arr
;;

let arr_c = calculate2 arr

let part1 arr p =
  let n = Array.length arr in
  let m = arr.(0) |> Array.length in
  let row_index = List.init m Fun.id in
  List.init n Fun.id
  |> List.fold_left
       (fun prev1 y ->
         row_index
         |> List.map (fun x ->
              List.exists (fun p -> p.(y).(x) < arr.(y).(x)) p |> int_of_bool)
         |> List.fold_left ( + ) prev1)
       0
;;

part1 arr res

module SS = Set.Make (Int)
module IntMap = Map.Make (Int)

let t = SS.find_first
let t = IntMap.find
