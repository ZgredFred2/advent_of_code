let result = Advent.read_lines "input/01.in"

(* pack_lines ["1"; "2"; "3"; ""; "4"; "5"; ""; "6"; "7"] "" *)
(* = [["1"; "2"; "3"]; ["4"; "5"]; ["6"; "7"]]) *)
let rec pack_lines list delim =
  match list with
  | [] -> []
  | h :: t ->
    let rest = pack_lines t delim in
    if h = delim then
      [] :: rest
    else (
      match rest with
      | hres :: tres -> (h :: hres) :: tres
      | [] -> [[h]]
    )
;;

let pack = pack_lines result "" |> List.map (List.map int_of_string)

(* let plus a b = a + b *)
let list_of_sums = List.map (List.fold_left ( + ) 0) pack
let sum_elems = List.fold_left ( + ) 0
let max_elem = List.fold_left max 0

(* List.fold_left ( + ) 0 [1; 2; 3] *)

(* insertSort [1;2;4] 3 = [1;2;3;4]*)
let rec insertSort list elem max_f =
  let aux list elem =
    match list with
    | [] -> [elem]
    | h :: t ->
      let m = max_f h elem in
      if h = m then
        elem :: list
      else
        h :: insertSort t elem max_f
  in
  aux list elem
;;

let fixed_insert max_f list elem = List.tl (insertSort list elem max_f)

let take_k_best max_f k list =
  List.fold_left (fixed_insert max_f) (List.init k (fun _ -> 0)) list
;;

let part1 = max_elem list_of_sums
let part2 = sum_elems (take_k_best max 3 list_of_sums)
