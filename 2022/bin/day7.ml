module StringMap = Map.Make (String)

let input = Advent.read_lines "input/07.in"
let split_by_space = Str.split (Str.regexp " ")

module Comm = struct
  type t =
    | Cd of { destination: string }
    | Ls

  let of_string : string -> t =
   fun s ->
    match split_by_space s with
    (* $ cd e *)
    | _dolar :: comm :: destination :: _ when comm = "cd" -> Cd { destination } (* $ ls *)
    | _dolar :: comm :: _ when comm = "ls" -> Ls
    | _ -> raise (Invalid_argument "invalid command string")
 ;;
end

module PrintResult = struct
  type t =
    | File of {
        size: int;
        name: string;
      }
    | Dir of { name: string }

  let of_string : string -> t =
   fun s ->
    match split_by_space s with
    (* dir a *)
    | dir_s :: name :: _ when dir_s = "dir" -> Dir { name }
    (* 2557 g *)
    | size_s :: name :: _ -> File { size = int_of_string size_s; name }
    | _ -> raise (Invalid_argument "invalid result description string")
 ;;
end

type termType =
  | CommPrompt of Comm.t
  | CommResult of PrintResult.t list

type lineType =
  | Comm of Comm.t
  | Result of PrintResult.t

let rec parse_lines lines =
  let parse_line line =
    match split_by_space line with
    | dolar_s :: _ when dolar_s = "$" -> Comm (Comm.of_string line)
    | _fst :: _snd :: _ -> Result (PrintResult.of_string line)
    | _ -> raise (Invalid_argument "invalid command line")
  in
  match lines with
  | [] -> []
  | h :: t -> parse_line h :: parse_lines t
;;

let rec pack_results results =
  match results with
  | [] -> []
  | Comm c :: t -> CommPrompt c :: pack_results t
  | Result r :: t ->
    let packed_rest = pack_results t in
    (match packed_rest with
     | [] -> [CommResult [r]]
     | CommResult comm_res_head :: rest -> CommResult (r :: comm_res_head) :: rest
     | rest_head :: rest_tail -> CommResult [r] :: rest_head :: rest_tail)
;;

let coms = input |> parse_lines |> pack_results

(*
parse: term_lines -> Dir -> (term_lines, Dir)
update_dir current_dir print_result
or
select_dir: StringMap -> string -> StingMap
select_dir map relative_dir

if comm = Cd
StringMap.add dir (parse rest_lines (select_dir dir)) m
if comm = Ls
update_dir ls_result m
 *)
(* [Cd; Ls; LsRes; Cd; Ls; Cd; Ls; LsRes; Cd; Cd] *)

type 'a nodeType =
  | Directory of { map: 'a }
  | File of { size: int }

type system_node = {
  name: string;
  opt: system_node StringMap.t nodeType;
}

let insert_if_empty map key value =
  let e = StringMap.find_opt key map in
  match e with
  | None -> StringMap.add key value map
  | Some _ -> map
;;

let rec update_dir { name = node_name; opt } (ls_result : PrintResult.t list) =
  let () = print_endline ("upadte_dir -> " ^ node_name) in
  match opt with
  | File _ -> raise (Invalid_argument "cannot update file")
  | Directory d ->
    (match ls_result with
     | [] -> { name = node_name; opt }
     | h :: t ->
       (match h with
        | PrintResult.Dir { name } ->
          let () = print_endline ("insert dir " ^ name) in
          let map =
            insert_if_empty d.map name { name; opt = Directory { map = StringMap.empty } }
          in
          update_dir { name = node_name; opt = Directory { map } } t
        | PrintResult.File { name; size } ->
          let () = print_endline ("insert file " ^ name) in
          let map = insert_if_empty d.map name { name; opt = File { size } } in
          update_dir { name = node_name; opt = Directory { map } } t))
;;

let print_node n =
  (* n - node, p - padding left *)
  let print_padding p = print_string (String.init p (fun _ -> ' ')) in
  let rec aux (n : system_node) p =
    let () = print_padding p in
    match n.opt with
    | File { size } -> print_endline (n.name ^ " " ^ string_of_int size)
    | Directory d ->
      let () = print_endline (n.name ^ "/") in
      StringMap.iter (fun _k v -> aux v (p + 4)) d.map
  in
  aux n 0
;;

let rec applyComs coms (main_node : system_node) =
  let () = print_endline ("entering " ^ main_node.name) in
  match main_node.opt with
  | File _ -> raise (Invalid_argument "cannot apply commands on file")
  | Directory dir ->
    (match coms with
     | [] -> [], main_node
     | CommPrompt (Cd { destination }) :: rest_coms as total_coms ->
       (match destination with
        | ".." when main_node.name = "/" ->
          let () = print_endline ("we " ^ main_node.name ^ " root") in
          applyComs rest_coms main_node
        | ".." ->
          let () = print_endline ("we " ^ main_node.name ^ " going back") in
          rest_coms, main_node
        | "/" when main_node.name = "/" ->
          let () = print_endline ("we " ^ main_node.name ^ " stay") in
          applyComs rest_coms main_node
        | "/" -> total_coms, main_node
        | destination ->
          let node =
            match StringMap.find_opt destination dir.map with
            (* make node*)
            | None -> { name = destination; opt = Directory { map = StringMap.empty } }
            (* if exist return that *)
            | Some ({ name = _; opt = Directory { map = _ } } as res) ->
              res (* possibly error *)
            | Some { name = _; opt = File { size = _ } } ->
              raise (Invalid_argument "destinaiton is file")
          in
          (* apply next commands to child *)
          let rest_coms, child_node = applyComs rest_coms node in
          let () = print_node child_node in
          (* use rest coms for yourself *)
          if rest_coms = [] then
            (* return self *)
            ( rest_coms,
              {
                name = main_node.name;
                opt = Directory { map = StringMap.add destination child_node dir.map };
              } )
          else
            applyComs
              rest_coms
              {
                name = main_node.name;
                opt = Directory { map = StringMap.add destination child_node dir.map };
              })
     | CommPrompt Ls :: CommResult ls_result :: rest_coms ->
       let updated_dir = update_dir main_node ls_result in
       let () = print_endline "updated node:" in
       let () = print_node updated_dir in
       applyComs rest_coms updated_dir
     | CommPrompt Ls :: _ -> raise (Invalid_argument "no ls result")
     | CommResult _ :: _ -> raise (Invalid_argument "ls result appear before ls comm"))
;;

let rootDir = { name = "/"; opt = Directory { map = StringMap.empty } }
let coms, root = applyComs coms rootDir
let () = print_endline "builded root"
let () = print_node root
let treshold = 100000

let solver node =
  let rec dfs node =
    match node.opt with
    | File { size } -> size, 0
    | Directory { map } ->
      let sum =
        StringMap.fold
          (fun _key curr (prev_sum, prev_res) ->
            let sum, res = dfs curr in
            ( prev_sum + sum,
              prev_res
              + res
              +
              match curr.opt with
              | File _ -> 0
              | Directory _ ->
                if sum < treshold then (
                  let () = print_endline ("take " ^ string_of_int sum) in
                  sum
                ) else
                  0 ))
          map
          (0, 0)
      in
      sum
  in
  dfs node
;;

let sum_of_root, part1 = solver root
let total_size = 70000000
let free_size = total_size - sum_of_root
let space_to_free = 30000000 - free_size

let solver node =
  let rec dfs node =
    match node.opt with
    | File { size } -> size, None
    | Directory { map } ->
      let sum =
        StringMap.fold
          (fun _key curr (prev_sum, prev_best) ->
            let sum, best = dfs curr in
            let best =
              match best with
              | Some x -> Some x
              | None ->
                (match sum - space_to_free with
                 | x when x >= 0 -> Some x
                 | _ -> None)
            in
            let () =
              print_endline
                ("subject "
                ^ curr.name
                ^ " best = "
                ^
                match best with
                | None -> "None"
                | Some x -> string_of_int x)
            in
            ( prev_sum + sum,
              match curr.opt with
              | File _ -> prev_best
              | Directory _ ->
                let () =
                  print_endline
                    (match best with
                     | None -> "None"
                     | Some x -> string_of_int x)
                in
                (match best, prev_best with
                 | None, prev -> prev
                 | best, None -> best
                 | Some a, Some b ->
                   (match a - space_to_free, b - space_to_free with
                    | a, b when a >= 0 && b >= 0 -> Some (min a b)
                    | a, _ when a >= 0 -> Some a
                    | _, b when b >= 0 -> Some b
                    | _ -> None)) ))
          map
          (0, None)
      in
      sum
  in

  dfs node
;;

let sum, sum_part2 = solver root

let part2 =
  match sum_part2 with
  | None -> -1
  | Some x -> x
;;

part2 + space_to_free
