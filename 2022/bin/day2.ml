(* Join the priv leaderboard by joining ThePrimeagen discord: https://discord.gg/ThePrimeagen *)

(* Shoutout to: https://buttondown.email/anmonteiro for the help *)
let input = Advent.read_lines "input/02.in"
let plus a b = a + b
let compare_as a b = max a (List.fold_left plus 0 b)
let sum_elems = List.fold_left plus 0
let max_elem = List.fold_left max 0

type selected =
  | Rock
  | Paper
  | Scissors

let stringToSelect s =
  match s with
  | "A"
   |"X" ->
    Rock
  | "B"
   |"Y" ->
    Paper
  | "C"
   |"Z" ->
    Scissors
  | _ -> raise Not_found
;;

module M = Map.Make (struct
  type t = selected

  let compare = compare
end)

let elems = [Scissors, Paper; Paper, Rock; Rock, Scissors]

(** return elemnt that [key] is winning with 
    eg. [win][[Scissors]][=Paper]
 *)
let win = elems |> List.to_seq |> M.of_seq

let lose =
  List.map
    (fun a ->
      let x, y = a in
      y, x)
    elems
  |> List.to_seq
  |> M.of_seq
;;

(* M.find Rock win;; *)
(* M.find Scissors win;; *)
(* M.find Paper win;; *)
(* M.find Rock lose;; *)
(* M.find Scissors lose;; *)
(* M.find Paper lose *)

let select_bonus e =
  match e with
  | Rock -> 1
  | Paper -> 2
  | Scissors -> 3
;;

type outcome =
  | Lose
  | Draw
  | Win

let outcome_bonus o =
  match o with
  | Lose -> 0
  | Draw -> 3
  | Win -> 6
;;

(* selects_to_outcome Rock Scissors -> Win *)
(* selects_to_outcome Rock Paper    -> Lose *)
let selects_to_outcome my opp =
  let won = M.find my win = opp in
  if won = true then
    Win
  else (
    let lose = M.find opp win = my in
    if lose = true then
      Lose
    else
      Draw
  )
;;

let countPoints my opp =
  let outcome = selects_to_outcome my opp in
  outcome_bonus outcome + select_bonus my
;;

let infer_selection opp outcome =
  match outcome with
  | Draw -> opp
  | Win -> M.find opp lose
  | Lose -> M.find opp win
;;

let outcone_of_string = function
  | "X" -> Lose
  | "Y" -> Draw
  | "Z" -> Win
  | _ -> raise Not_found
;;

let part1 =
  (* ["A X"; "B Y"; "C Z"]*)
  input
  (* [["A"; "X"]; ["B"; "Y"]; ["C"; "Z"]] *)
  |> List.map (String.split_on_char ' ')
  (* [(Rock, Paper); (Scizors, Rock); (Paper, Scissors)] *)
  |> List.map (function
       | a :: b :: _ -> stringToSelect a, stringToSelect b
       | _ -> raise Not_found)
  (* [1; 2; 3]*)
  |> List.map (fun (opp, me) -> countPoints me opp)
  (* 6 *)
  |> List.fold_left ( + ) 0
;;

let part2 =
  (* ["A X"; "B Y"; "C Z"]*)
  input
  (* [["A"; "X"]; ["B"; "Y"]; ["C"; "Z"]] *)
  |> List.map (String.split_on_char ' ')
  (* [(Rock, Win); (Scizors, Lose); (Paper, Draw)] *)
  |> List.map (function
       | a :: b :: _ -> stringToSelect a, outcone_of_string b
       | _ -> raise Not_found)
  (* [1; 2; 3]*)
  |> List.map (fun (opp, outcome) -> countPoints (infer_selection opp outcome) opp)
  (* 6 *)
  |> List.fold_left ( + ) 0
;;
