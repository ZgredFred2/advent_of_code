let input = Advent.read_lines "input/06.in"
let split_by_space = Str.split (Str.regexp " ")

module Make (Ord : Map.OrderedType) = struct
  include Map.Make (Ord)

  let find_or_default key default map =
    match find_opt key map with
    | None -> default
    | Some x -> x
  ;;

  let modify_under_key key modify default map =
    let curr = find_or_default key default map in
    let new_val = modify curr in
    add key new_val map
  ;;

  let replace_with_default key value map =
    match find_opt key map with
    | None -> map |> add key value
    | Some x -> map |> add key value
  ;;
end

module IntMap = Make (Int)
module CharMap = Make (Char)
module StringMap = Make (String)

let rec extract list n =
  let rec aux list n =
    match list with
    | [] ->
      raise
        (Invalid_argument (string_of_int n ^ " is greather than list length-1 with list="))
    | h :: t ->
      if n = 0 then
        h, t
      else (
        let selected, rest = aux t (n - 1) in
        selected, h :: rest
      )
  in
  aux list n
;;

(* let () = assert (extract [0; 1; 2; 3] 3 = (3, [0; 1; 2])) *)

let cl2s cl = String.concat "" (List.map (String.make 1) cl)

let rec solver s span_width =
  let satisfy char_map = CharMap.for_all (fun _ v -> v <= 1) char_map in
  let rec aux s save (curr_count, dest_count) char_map =
    match s with
    | [] -> raise (Invalid_argument "not such point in word")
    | h :: t ->
      (match curr_count, dest_count with
       | a, b when a < b ->
         let char_map = CharMap.modify_under_key h (( + ) 1) 0 char_map in
         if a + 1 = b && satisfy char_map then
           1
         else
           aux t (h :: save) (curr_count + 1, dest_count) char_map + 1
       | a, b when a = b ->
         let char_to_remove, rest_saved_word = extract save (List.length save - 1) in
         let char_map = CharMap.modify_under_key h (( + ) 1) 0 char_map in
         let char_map = CharMap.modify_under_key char_to_remove (( + ) (-1)) 0 char_map in
         if satisfy char_map then
           1
         else
           aux t (h :: rest_saved_word) (a, b) char_map + 1
       | _ -> raise (Invalid_argument "back stacks"))
  in
  aux s [] (0, span_width) CharMap.empty
;;

let string_to_char_list s = List.init (String.length s) (String.get s);;

input;;
input |> List.map string_to_char_list |> List.map (fun s -> solver s 14)
