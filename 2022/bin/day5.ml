(* Join the priv leaderboard by joining ThePrimeagen discord: https://discord.gg/ThePrimeagen *)

(* Shoutout to: https://buttondown.email/anmonteiro for the help *)
let input = Advent.read_lines "input/05.in"

module IntMap = Map.Make (Int)

(* pack_lines ["1"; "2"; "3"; ""; "4"; "5"; ""; "6"; "7"] "" *)
(* = [["1"; "2"; "3"]; ["4"; "5"]; ["6"; "7"]]) *)
let rec pack_lines list delim =
  match list with
  | [] -> []
  | h :: t ->
    let rest = pack_lines t delim in
    if h = delim then
      [] :: rest
    else (
      match rest with
      | hres :: tres -> (h :: hres) :: tres
      | [] -> [[h]]
    )
;;

let rec extract list n =
  match list with
  | [] -> raise Not_found
  | h :: t ->
    if n = 0 then
      h, t
    else (
      let selected, rest = extract t (n - 1) in
      selected, h :: rest
    )
;;

let (a :: b :: _) = pack_lines input ""

(* Split a list into two parts; the length of the first part is given. *)
(* If the length of the first part is longer than the entire list, then the first part is the list and the second part is empty. *)
let rec split list n =
  let rec aux list n acc =
    match list with
    | [] -> acc, []
    | h :: t ->
      if n = 0 then
        acc, list
      else
        aux t (n - 1) (h :: acc)
  in
  let f, s = aux list n [] in
  List.rev f, s
;;

(** Return elements of [list] that positions are included in [index_list]
    The first element (head of the list) is at position 0.
    [index_list] has to be sorted.
    [index_list] can include positions that don't exist in [list]
 *)
let rec take_indexes list index_list =
  let rec aux list index_list current_pos acc =
    match index_list with
    | [] -> acc
    | index_head :: index_tail ->
      (match list with
       | [] -> acc
       | list_head :: list_tail ->
         if current_pos = index_head then
           (* include head and go further in index_list *)
           aux list_tail index_tail (current_pos + 1) (list_head :: acc)
         else
           (* go further in list *)
           aux list_tail index_list (current_pos + 1) acc)
  in
  let aux_res = aux list index_list 0 [] in
  List.rev aux_res
;;

module Move = struct
  type t = {
    origin: int;
    destination: int;
    count: int;
  }

  (* move 1 from 2 to 1*)
  let of_string : string -> t =
   fun s ->
    match Str.split (Str.regexp " ") s with
    | _move :: count :: _from :: origin :: _to :: destination :: _ ->
      let count, origin, destination =
        int_of_string count, int_of_string origin, int_of_string destination
      in
      { origin; destination; count }
    | _ -> assert false
 ;;

  let to_string (m : t) = Printf.sprintf "move (%d) %d->%d" m.count m.origin m.destination
  let compare a b = compare (to_string a) (to_string b)
end

let string_to_char_list s = List.init (String.length s) (String.get s)

(* ["[z] [m] [p]"] -> [z;m;p]*)
let extract_symbols list =
  take_indexes list (List.init ((List.length list + 1) / 4) (fun idx -> (idx * 4) + 1))
;;

let symbols = a |> List.map string_to_char_list |> List.map extract_symbols

let (_, a), moves, c =
  let l = List.length symbols in
  extract symbols (l - 1), b, List.nth symbols (l - 1)
;;

let rec connect_vertically list =
  match list with
  | [] -> []
  | h :: _ when h = [] -> []
  | _ :: _ ->
    let stack, rest =
      list
      (* |> List.map (function *)
      (*      | h :: t -> h, t *)
      (*      | [] -> raise Not_found) *)
      |> List.fold_left
           (fun (line_stack, rest_list) curr ->
             match curr with
             | [] -> line_stack, rest_list
             | h :: t -> h :: line_stack, t :: rest_list)
           ([], [])
    in
    if rest = [] then
      [List.rev stack]
    else
      List.rev stack :: connect_vertically (List.rev rest)
;;

let stack_list = connect_vertically a

(* make_stack [[' '; 'N'; 'Z']; ['D'; 'C'; 'M']; [' '; ' '; 'P']] ['1'; '2'; '3'] =  *)
let rec make_stack list nums =
  match list with
  | [] -> IntMap.empty
  | stack :: stack_tail ->
    (match nums with
     | [] -> raise (Invalid_argument "shoud be qual length")
     | num :: num_tail ->
       let stack =
         String.of_seq (List.to_seq stack) |> String.trim |> string_to_char_list
       in
       IntMap.add num stack (make_stack stack_tail num_tail))
;;

let c = c |> List.map (fun a -> int_of_string (String.make 1 a))
let s = make_stack stack_list c

type map = char list IntMap.t

let pp_map ppf (m : map) =
  IntMap.iter
    (fun k v -> Format.fprintf ppf "%d -> %s@\n" k (String.of_seq (List.to_seq v)))
    m
;;

let print_map m = Format.printf "%a" pp_map m;;

moves;;
print_map s;;

(* part 1*)
moves
|> List.map Move.of_string
|> List.fold_left
     (fun m (move : Move.t) ->
       let origin_stack, destination_stack =
         IntMap.find move.origin m, IntMap.find move.destination m
       in
       let taken_stack, origin_rest_stack = split origin_stack move.count in
       let origin_stack, destination_stack =
         origin_rest_stack, List.rev taken_stack @ destination_stack
       in
       let res =
         m
         |> IntMap.add move.origin origin_stack
         |> IntMap.add move.destination destination_stack
       in
       (* let () = print_map m in *)
       res)
     s
|> print_map
;;

(* part 2*)

moves
|> List.map Move.of_string
|> List.fold_left
     (fun m (move : Move.t) ->
       let origin_stack, destination_stack =
         IntMap.find move.origin m, IntMap.find move.destination m
       in
       let taken_stack, origin_rest_stack = split origin_stack move.count in
       let origin_stack, destination_stack =
         origin_rest_stack, taken_stack @ destination_stack
       in
       let res =
         m
         |> IntMap.add move.origin origin_stack
         |> IntMap.add move.destination destination_stack
       in
       (* let () = print_map m in *)
       res)
     s
|> print_map
