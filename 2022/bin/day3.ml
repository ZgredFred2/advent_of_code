(* Join the priv leaderboard by joining ThePrimeagen discord: https://discord.gg/ThePrimeagen *)

(* Shoutout to: https://buttondown.email/anmonteiro for the help *)
let input = Advent.read_lines "input/03.in"
let plus a b = a + b
let compare_as a b = max a (List.fold_left plus 0 b)
let sum_elems = List.fold_left plus 0
let max_elem = List.fold_left max 0

module M = Map.Make (Char)

let rec split list n =
  let rec aux list n acc =
    match list with
    | [] -> acc, []
    | h :: t ->
      if n = 0 then
        acc, list
      else
        aux t (n - 1) (h :: acc)
  in
  let f, s = aux list n [] in
  List.rev f, s
;;

let string_to_char_list s = List.init (String.length s) (String.get s)
let find_uniqe l = List.fold_left (fun a b -> M.add b 1 a) M.empty l

(* now can be raplaced with find_common_in_group [l1;l2]*)
let find_common l1 l2 =
  let l1_map = List.fold_left (fun a b -> M.add b 1 a) M.empty l1 in
  let rec find e l =
    match l with
    | [] -> raise Not_found
    | h :: t ->
      if M.find_opt h e = Some 1 then
        h
      else
        find e t
  in
  find l1_map l2
;;

let find_common_in_group l =
  let rec reduce m1 m2 = M.filter (fun a _ -> M.find_opt a m2 = Some 1) m1 in
  l
  |> List.map find_uniqe
  |> List.fold_left reduce (find_uniqe (List.hd l))
  |> M.to_seq
  |> List.of_seq
  |> List.hd
  |> (fun (a, _) -> a)
;;

let solver s =
  let l = string_to_char_list s in
  let len = List.length l in
  let a, b = split l (len / 2) in
  find_common a b
;;

let points c =
  let base = Char.code 'a' - 1 in
  let lower_c = Char.lowercase_ascii c in
  let is_upper = c != lower_c in
  let base_points = Char.code lower_c - base in
  base_points
  +
  if is_upper then
    (* A -> a + 26 -> 1 + 26 -> 27*)
    26
  else
    0
;;

let part1 = input |> List.map solver |> List.map points |> List.fold_left ( + ) 0

let part2 =
  input
  |> List.map string_to_char_list
  |> (let rec aux = function
        | a :: b :: c :: t -> find_common_in_group [a; b; c] :: aux t
        | _ -> []
      in
      aux)
  |> List.map points
  |> List.fold_left ( + ) 0
;;
