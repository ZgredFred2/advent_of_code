let input = Advent.read_lines "input/04.in"
let split_by_space = Str.split (Str.regexp " ")

module Assignment = struct
  type t = (int * int) * (int * int)

  let of_string line =
    match
      line |> Str.global_replace (Str.regexp "-") "," |> Str.split (Str.regexp ",")
    with
    | s1 :: e1 :: s2 :: e2 :: _ ->
      (int_of_string s1, int_of_string e1), (int_of_string s2, int_of_string e2)
    | _ -> raise (Invalid_argument "invalid line")
  ;;
end

let rec solve a b =
  let a_in_b (s1, e1) (s2, e2) = s1 <= s2 && e2 <= e1 in
  let poin_in p (s, e) = s <= p && p <= e in

  match a, b with
  | (s1, e1), (s2, e2) when a_in_b a b || a_in_b b a -> 1
  | _ -> 0
;;

let part1 =
  input
  |> List.map Assignment.of_string
  |> List.map (fun (a, b) -> solve a b)
  |> List.fold_left ( + ) 0
;;

let rec solve a b =
  let poin_in p (s, e) = s <= p && p <= e in
  let intersect (s, e) b = poin_in s b || poin_in e b in

  match a, b with
  | a, b when intersect a b || intersect b a -> 1
  | _ -> 0
;;

let part2 =
  input
  |> List.map Assignment.of_string
  |> List.map (fun (a, b) -> solve a b)
  |> List.fold_left ( + ) 0
;;
